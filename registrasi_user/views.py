from django.shortcuts import render, redirect
from .forms import LoginForm, AdminForm, DokterForm, PasienForm, AlergiForm
from django.forms import formset_factory
from django.db import connection
from random import randint

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def session_adders(request, username):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    try:
        cursor.execute(f"SELECT username FROM Administrator WHERE username = '{username}'")
        hasil = dictfetchall(cursor)[0]
        role = "ADMIN"
    except:
        try:
            cursor.execute(f"SELECT username FROM Dokter WHERE username = '{username}'")
            hasil = dictfetchall(cursor)[0]
            role = "DOKTER"
        except:
            role = "PASIEN"
    request.session.modified = True
    request.session['role'] = role 
    request.session['username'] = username

def homepage_redirector(request):
    # Doesnt Work somehow
    if request.session.get('role') == "DOKTER":
        return redirect("/sesi-konsultasi/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/sesi-konsultasi/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/sesi-konsultasi/")

def login_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    
    if request.session.get('role') == "DOKTER":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/profil/" + request.session.get('username') + "/")
    
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            cursor.execute("select username, password from pengguna where username = '" + username + "' and password = '" + password + "';")
            try:
                user = dictfetchall(cursor)[0]
            except:
                context = {
                    'form' : form,
                    'error' : "Username/password tidak valid"
                }
                return render(request, 'registrasi_user/login.html', context)
            if user['username'] != None and user['password'] != None:
                session_adders(request, username)
                return redirect("/profil/" + request.session.get('username') + "/")
            else:
                context = {
                    'form' : form,
                    'error' : "Username/password tidak valid"
                }
                return render(request, 'registrasi_user/login.html', context)
        else:
            context = {
                'form' : form
            }
            return render(request, 'registrasi_user/login.html', context)
    else:
        form = LoginForm()
        context = {
            'form' : form
        }
        return render(request, 'registrasi_user/login.html', context)

def role_view(request):
    if request.session.get('role') == "DOKTER":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/profil/" + request.session.get('username') + "/")
    return render(request, 'registrasi_user/role.html')

def admin_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    
    if request.session.get('role') == "DOKTER":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/profil/" + request.session.get('username') + "/")

    if request.method == 'POST':
        form = AdminForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nomor_identitas = form.cleaned_data.get('nomor_identitas')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            tanggal_lahir = form.cleaned_data.get('tanggal_lahir')
            email = form.cleaned_data.get('email')
            alamat = form.cleaned_data.get('alamat')
            try:
                cursor.execute("insert into pengguna(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) values('" + username + "', '" + password + "', '" + nomor_identitas + "', '" + nama_lengkap + "', '" + str(tanggal_lahir) + "', '" + email + "', '" + alamat + "');")
                # algoritma generate no_pegawai
                cursor.execute("select nomor_pegawai from administrator order by length(nomor_pegawai), nomor_pegawai")
                res_no_pegawai = dictfetchall(cursor)
                no_pegawai = int(res_no_pegawai[len(res_no_pegawai)-1]['nomor_pegawai']) + 1
                cursor.execute("insert into administrator(nomor_pegawai, username, kode_rs) values ('" + str(no_pegawai) + "', '" + username + "', '1');")
                session_adders(request, username)
                return redirect("/profil/" + request.session.get('username') + "/")
            except:
                error = "Administrator dengan username / email / no identitas tersebut sudah ada"
                context = {
                    'form' : form,
                    'error' : error
                }
                return render(request, 'registrasi_user/admin.html', context)
        else:
            context = {
                'form' : form
            }
            return render(request, 'registrasi_user/admin.html', context)
    else:
        form = AdminForm()
        context = {
            'form' : form
        }
        return render(request, 'registrasi_user/admin.html', context)

def dokter_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "DOKTER":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/profil/" + request.session.get('username') + "/")

    if request.method == 'POST':
        form = DokterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nomor_identitas = form.cleaned_data.get('nomor_identitas')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            tanggal_lahir = form.cleaned_data.get('tanggal_lahir')
            email = form.cleaned_data.get('email')
            alamat = form.cleaned_data.get('alamat')
            no_sip = form.cleaned_data.get('no_sip')
            spesialisasi = form.cleaned_data.get('spesialisasi')
            try:
                cursor.execute("insert into pengguna(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) values('" + username + "', '" + password + "', '" + nomor_identitas + "', '" + nama_lengkap + "', '" + str(tanggal_lahir) + "', '" + email + "', '" + alamat + "');")
                # algoritma generate id_dokter
                cursor.execute("select id_dokter from dokter order by length(id_dokter), id_dokter")
                res_id_dokter = dictfetchall(cursor)
                id_dokter = int(res_id_dokter[len(res_id_dokter)-1]['id_dokter']) + 1
                cursor.execute("insert into dokter(id_dokter, username, no_sip, spesialisasi) values ('" + str(id_dokter) + "', '" + username + "', '" + no_sip +  "', '" + spesialisasi + "');")
                session_adders(request, username)
                return redirect("/profil/" + request.session.get('username') + "/")
            except:
                error = "Dokter dengan username / email / no identitas tersebut sudah ada"
                context = {
                    'form' : form,
                    'error' : error
                }
                return render(request, 'registrasi_user/dokter.html', context)
        else:
            context = {
                'form' : form
            }
            return render(request, 'registrasi_user/dokter.html', context)
    else:
        form = DokterForm()
        context = {
            'form' : form
        }
        return render(request, 'registrasi_user/dokter.html', context)

def pasien_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    
    if request.session.get('role') == "DOKTER":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "ADMIN":
        return redirect("/profil/" + request.session.get('username') + "/")
    elif request.session.get('role') == "PASIEN":
        return redirect("/profil/" + request.session.get('username') + "/")

    DaftarAlergi = formset_factory(form=AlergiForm)
    if request.method == 'POST':
        form = PasienForm(request.POST)
        formset = DaftarAlergi(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            nomor_identitas = form.cleaned_data.get('nomor_identitas')
            nama_lengkap = form.cleaned_data.get('nama_lengkap')
            tanggal_lahir = form.cleaned_data.get('tanggal_lahir')
            email = form.cleaned_data.get('email')
            alamat = form.cleaned_data.get('alamat')
            list_alergi = []
            number = 0
            while True:
                try:
                    tag = 'form-' + str(number) + '-alergi'
                    alergi = request.POST[tag]
                    if len(alergi) != 0:
                        list_alergi.append(alergi)
                    number += 1
                except:
                    break
            try:
                cursor.execute("insert into pengguna(username, password, nomor_id, nama_lengkap, tanggal_lahir, email, alamat) values('" + username + "', '" + password + "', '" + nomor_identitas + "', '" + nama_lengkap + "', '" + str(tanggal_lahir) + "', '" + email + "', '" + alamat + "');")
                # algoritma generate no_rekam_medis
                cursor.execute("select no_rekam_medis from pasien order by length(no_rekam_medis), no_rekam_medis")
                res_no_rekam_medis = dictfetchall(cursor)
                no_rekam_medis = res_no_rekam_medis[len(res_no_rekam_medis)-1]['no_rekam_medis']
                no_rekam_medis_baru = no_rekam_medis[0:10] + str(int(no_rekam_medis[10]) + 1)
                cursor.execute("select nama from asuransi;")
                insurances = dictfetchall(cursor)
                asuransi = insurances[randint(0,9)]['nama']
                cursor.execute("insert into pasien(no_rekam_medis, username, nama_asuransi) values ('" + no_rekam_medis_baru + "', '" + username + "', '" + asuransi  + "');")
                for alergi in list_alergi:
                    cursor.execute("insert into alergi_pasien(no_rekam_medis, alergi) values ('" + no_rekam_medis_baru + "', '" + alergi + "');")
                session_adders(request, username)
                return redirect("/profil/" + request.session.get('username') + "/")
            except:
                error = "Pasien dengan username / email / no identitas tersebut sudah ada"
                context = {
                    'form' : form,
                    'error' : error
                }
                return render(request, 'registrasi_user/pasien.html', context)
        else:
            context = {
                'form' : form,
                'formset' : formset
            }
            return render(request, 'registrasi_user/pasien.html', context)
    else:
        form = PasienForm()
        formset = DaftarAlergi()
        context = {
            'form' : form,
            'formset' : formset
        }
        return render(request, 'registrasi_user/pasien.html', context)

def logout_view(request):
    try:
        if request.session['username'] != None:
            del request.session['username']
            del request.session['role']
            return redirect("/")
        else:
            return redirect("/")
    except:
        return redirect("/")