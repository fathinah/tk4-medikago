from django.urls import path
from . import views
from .views import login_view, role_view, admin_view, dokter_view, pasien_view, logout_view

appname = 'registrasi_user'

urlpatterns = [
    path('', login_view, name='login'),
    path('role/', role_view, name='role'),
    path('logout/', logout_view, name='logout'),
    path('create-admin/', admin_view, name='admin'),
    path('create-dokter/', dokter_view, name='dokter'),
    path('create-pasien/', pasien_view, name='pasien')
]