$(document).ready(function(){
    var number = 0;
    var filled_alergies = [];
    $('.add_form').click(function(e){
        var filled = $("#id_form-" + number + "-alergi").val();
        if ($.inArray(filled, filled_alergies) == -1) {
            if (filled == "") {
                alert(`Isi input dulu sebelum menambah!`);
            }
            else {
                filled_alergies.push(filled);
                $("#id_form-" + number + "-alergi").prop('readonly', true);
                number += 1
                var form = `<input type="text" name="form-` + number + `-alergi" id="id_form-` + number + `-alergi">`;
            }
        }
        else {
            alert(`Alergi tidak boleh ada yang sama!`);
        }
        console.log(filled_alergies)
        $('.link_formset').append(form);
    });
    $('.delete_form').click(function(e){
        if (number > 0) {
            var filled = $("#id_form-" + number + "-alergi").val();
            filled_alergies.pop(filled);
            $("#id_form-" + number + "-alergi").remove();
            number -= 1
            $("#id_form-" + number + "-alergi").prop('readonly', false);
        }
    });
});