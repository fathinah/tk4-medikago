from django.urls import path
from django.conf.urls import url
from .views import sesi_konsultasi, buat_sesi_konsultasi, update_sesi_konsultasi, delete_sesi_konsultasi

app_name = 'sesi_konsultasi'

urlpatterns = [
	path('buat-sesi-konsultasi/', buat_sesi_konsultasi, name='buat_sesi_konsultasi'),
	path('sesi-konsultasi/', sesi_konsultasi, name='daftar_sesi_konsultasi'),
	url(r'^update-sesi-konsultasi/(?P<slug>[\w-]+)/$', update_sesi_konsultasi, name='update_sesi_konsultasi'),
	url(r'^delete-sesi-konsultasi/(?P<slug>[\w-]+)/$', delete_sesi_konsultasi, name='delete_sesi_konsultasi'),
]
