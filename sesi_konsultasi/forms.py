from django import forms
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

class BuatSesiKonsultasi(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago;')
        cursor.execute('select no_rekam_medis from pasien;')
        dict_nomor_rekam_medis_pasien = dictfetchall(cursor)
        NOMOR_REKAM_MEDIS_CHOICES = [tuple([row['no_rekam_medis'], row['no_rekam_medis']]) for row in dict_nomor_rekam_medis_pasien]
        self.fields['Nomor_Rekam_Medis_Pasien'].choices = NOMOR_REKAM_MEDIS_CHOICES
        cursor = connection.cursor()
        cursor.execute('set search_path to medikago;')
        cursor.execute('select id_transaksi from transaksi;')
        dict_id_transaksi = dictfetchall(cursor)
        ID_TRANSAKSI_CHOICES = [tuple([row['id_transaksi'], row['id_transaksi']]) for row in dict_id_transaksi]
        self.fields['ID_Transaksi'].choices = ID_TRANSAKSI_CHOICES
        
    Nomor_Rekam_Medis_Pasien = forms.ChoiceField(required=True, widget=forms.Select())

    Tanggal = forms.DateField(widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))

    ID_Transaksi = forms.ChoiceField(required=True, widget=forms.Select())

class UpdateSesiKonsultasi(forms.Form):
    ID_Konsultasi = forms.CharField(widget=forms.TextInput(attrs={
        'readonly' : True
    }))

    Nomor_Rekam_Medis_Pasien = forms.CharField(widget=forms.TextInput(attrs={
        'readonly' : True
    }))

    Tanggal = forms.DateField(widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))

    Biaya = forms.CharField(widget=forms.NumberInput(attrs={
        'readonly' : True
    }))

    Status = forms.CharField(max_length = 50, widget=forms.TextInput(attrs={
        'type' : 'text',
        'required': True,
    }))

    ID_Transaksi = forms.CharField(widget=forms.TextInput(attrs={
        'readonly' : True
    }))
