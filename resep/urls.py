from django.conf.urls import url
from django.urls import path
from . import views
from .views import addresep_view, daftarresep_view, delete_resep_view

appname = 'resep'

urlpatterns = [
    path('create-resep/', addresep_view, name='create resep'),
    path('daftar-resep/', daftarresep_view, name='daftar resep'),
    url(r'^delete-resep/(?P<slug>[\w-]+)/$', delete_resep_view, name='delete resep')
]