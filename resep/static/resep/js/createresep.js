$(document).ready(function(){
    var number = 0
    var datas = []
    var childs = $("#id_form-" + number + "-daftar_kode_obat").children();
    for (i = 0; i < childs.length; i++) {
        datas.push(childs[i].text);
    }
    var selections = []
    $('.add_form').click(function(e){
        var selected = $("#id_form-" + number + "-daftar_kode_obat option:selected").text();
        selections.push(selected);
        number += 1
        var form = `<select name="form-` + number + `-daftar_kode_obat" id="id_form-` + number + `-daftar_kode_obat">`;
        var checker = 0
        for (i = 0; i < datas.length; i++) {
            if($.inArray(datas[i], selections) == -1) {
                form += `<option value="` + datas[i] +`">` + datas[i] + `</option>`;
                checker += 1;
            }
        }
        form += `</select>`;
        if(checker > 0) {
            $('.link_formset').append(form);
        }
        else {
            alert(`Semua obat telat ditambahkan!`);
        }
    });
});