from django.shortcuts import render, redirect
from django.db import connection
from random import randint
from django.http import HttpResponse

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def addresep_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    cursor.execute("select id_konsultasi from sesi_konsultasi;")
    id_konsultasi_rows = dictfetchall(cursor)
    DAFTAR_KONSULTASI = [konsultasi["id_konsultasi"] for konsultasi in id_konsultasi_rows]

    cursor.execute("select id_transaksi from transaksi;")
    id_transaksi_rows = dictfetchall(cursor)
    DAFTAR_TRANSAKSI = [transaksi['id_transaksi'] for transaksi in id_transaksi_rows]

    cursor.execute("select kode from obat;")
    kode_obat_rows = dictfetchall(cursor)
    DAFTAR_OBAT= [kode_obat['kode'] for kode_obat in kode_obat_rows]

    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            id_konsultasi = request.POST['id_konsultasi']
            id_transaksi = request.POST['id_transaksi']
            list_kode_obat = []
            number = 0
            while True:
                try:
                    tag = 'form-' + str(number) + '-daftar_kode_obat'
                    kode_obat = str(request.POST[tag])
                    if len(kode_obat) != 0 and kode_obat not in list_kode_obat:
                        list_kode_obat.append(kode_obat)
                    number += 1
                except:
                    break
            # algoritma generate no_resep
            cursor.execute("select no_resep from resep order by length(no_resep), no_resep;")
            res = dictfetchall(cursor)
            no_resep = int(res[len(res) - 1]['no_resep']) + 1
            cursor.execute("insert into resep(no_resep, id_konsultasi, total_harga, id_transaksi) values ('" + str(no_resep) + "', '" + id_konsultasi + "', " + str(0) + ", '" + id_transaksi + "');")
            for kode in list_kode_obat:
                cursor.execute("select bentuk_sediaan from obat where kode='" + str(kode) + "';")
                obatnya = dictfetchall(cursor)
                dosis = str(randint(1, 10)) + " " + obatnya[0]['bentuk_sediaan'].lower()
                aturan = str(randint(1, 4)) + " kali per hari"
                cursor.execute("insert into daftar_obat(no_resep, kode_obat, dosis, aturan) values ('" + str(no_resep) + "', '" + kode + "', '" + dosis + "', '" + aturan + "');")
            return redirect('/daftar-resep/')
        else:
            context = {
                'daftar_konsultasi' : DAFTAR_KONSULTASI,
                'daftar_obat' : DAFTAR_OBAT,
                'daftar_transaksi' : DAFTAR_TRANSAKSI
            }
            return render(request, 'resep/createresep.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def daftarresep_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        role = "admin"
    else:
        role = "others"
    
    cursor.execute("select * from resep order by length(no_resep), no_resep;")
    rows = dictfetchall(cursor)
    cursor.execute("select * from daftar_obat order by length(kode_obat), kode_obat;")
    daftar_obat = dictfetchall(cursor)
    return render(request, 'resep/daftarresep.html', {'rows':rows, 'daftar_obat':daftar_obat, 'role':role})

def delete_resep_view(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        cursor.execute("delete from resep where no_resep='" + str(slug) + "';")
        return redirect("/daftar-resep/")
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)