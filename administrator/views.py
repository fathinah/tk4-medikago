from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponse

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def admin_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    NO_PEGAWAI = []
    cursor.execute("select nomor_pegawai from administrator")
    no_pegawai = dictfetchall(cursor)
    for pegawai in no_pegawai:
        NO_PEGAWAI.append(pegawai['nomor_pegawai'])
    
    KODE_RS = []
    cursor.execute("select kode_rs from rs_cabang")
    rs_cabang = dictfetchall(cursor)
    for rs in rs_cabang:
        KODE_RS.append(rs['kode_rs'])
    
    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            no_pegawai = request.POST['no_pegawai']
            kode_rs = request.POST['kode_rs']
            cursor.execute("select kode_rs from administrator where kode_rs='" + kode_rs + "';")
            rs_checker = dictfetchall(cursor)
            print(rs_checker)
            if (kode_rs != '1'):
                if (len(rs_checker) >= 2):
                    context = {
                        'no_pegawai' : NO_PEGAWAI,
                        'kode_rs' : KODE_RS,
                        'error' : "Admin suatu RS Cabang maksimal 2!"
                    }
                    return render(request, 'administrator/administrator.html', context)
                else:
                    cursor.execute("update administrator set kode_rs='" + kode_rs + "' where nomor_pegawai='" + no_pegawai + "';")
                    return redirect("/rs-cabang/")
            else:
                cursor.execute("update administrator set kode_rs='" + kode_rs + "' where nomor_pegawai='" + no_pegawai + "';")
                return redirect("/rs-cabang/")
        else:
            context = {
                'no_pegawai' : NO_PEGAWAI,
                'kode_rs' : KODE_RS
            }
            return render(request, 'administrator/administrator.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)