from django.urls import path
from . import views
from .views import admin_view

appname = 'administrator'

urlpatterns = [
    path('set-admin/', admin_view, name='set admin')
]