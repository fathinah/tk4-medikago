from django.urls import path
from . import views
from .views import *

appname = 'transaksi'

urlpatterns = [
    path('daftar-transaksi/buat/', buat_transaksi_view, name='buat transaksi'),
    path('daftar-transaksi/',daftart_transaksi_view, name='daftar transaksi'),
    path('daftar-transaksi/update/', update_transaksi_view, name='update transaksi'),
    path('daftar-transaksi/hapus/', hapus_transaksi_view, name='hapus transaksi'),
]