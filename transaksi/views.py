from django.shortcuts import render, redirect
from .forms import BuatTransaksiForm, UpdateTransaksiForm
from django.db import connection
import datetime

cursor = connection.cursor()
cursor.execute('set search_path to medikago;')

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    # print(columns)
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

# Create your views here.
def buat_transaksi_view(request):
    if request.session.get('role') != "ADMIN":
        return redirect("/daftar-transaksi/")
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago;')
    if request.method == 'POST':
        form = BuatTransaksiForm(request.POST)
        if form.is_valid():
            cursor.execute("SET TIMEZONE='Asia/Jakarta';")
            cursor.execute('SELECT id_transaksi FROM Transaksi ORDER BY id_transaksi DESC LIMIT 1;')
            temp = dictfetchall(cursor)
            # Isi
            id_transaksi = str(int(temp[0]['id_transaksi']) + 1 )
            # tanggal = datetime.date.today()
            status = "'Created'"
            total_biaya = 0
            # waktu_pembayaran = datetime.datetime.now()
            no_rek_med = form.cleaned_data['no_rekam_medis']
            # Query masukin
            cursor.execute('INSERT INTO Transaksi (id_transaksi, tanggal, status, total_biaya, waktu_pembayaran, no_rekam_medis) values (' + 
                            id_transaksi + ',' + 'NOW()' + ',' + status + ',' + str(total_biaya) + ',' + 'NOW()' + ",'" + no_rek_med + "');")
        return redirect("/daftar-transaksi/")
    else:
        form = BuatTransaksiForm()
        lampiran = {
            'form' : form,
        }
        # to do, if success redirect ke daftar transaksi
        return render(request, 'buatTransaksi.html', lampiran)

def update_transaksi_view(request):
    if request.session.get('role') != "ADMIN":
        return redirect("/daftar-transaksi/")
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago;')
    if request.method == 'POST':
        form = UpdateTransaksiForm(request.POST)
        if form.is_valid():
            cursor.execute(f"UPDATE Transaksi SET tanggal = '{form.cleaned_data['tanggal']}', status = '{form.cleaned_data['status']}', waktu_pembayaran = '{form.cleaned_data['waktu_pembayaran']}' WHERE id_transaksi = '{form.cleaned_data['id_transaksi']}';")
            return redirect('/daftar-transaksi')
        else:
            lampiran = {
                'error' : 'error pak',
                'form' : form
            }
            return render(request, 'updateTransaksi.html', lampiran)
    else:
        cursor.execute(f"SELECT tanggal, waktu_pembayaran FROM Transaksi WHERE id_transaksi = '{request.GET['id_transaksi']}';")
        temp = dictfetchall(cursor)
        tgl = temp[0]['tanggal']
        wkt_byr = temp[0]['waktu_pembayaran']
        print(wkt_byr)
        form = UpdateTransaksiForm(initial={
            'id_transaksi' : request.GET['id_transaksi'],
            'tanggal' : tgl,
            'status' : request.GET['status'],
            'total_biaya': request.GET['total_biaya'],
            'waktu_pembayaran' : wkt_byr,
            'no_rekam_medis_pasien' : request.GET['no_rekam_medis'],
        })
        lampiran = {
            'form' : form,
        }
        return render(request, 'updateTransaksi.html', lampiran)

def daftart_transaksi_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago;')
    cursor.execute("SELECT * FROM Transaksi ORDER BY status ASC;")
    transaksi = dictfetchall(cursor)
    # print(transaksi)
    return render(request, 'daftarTransaksi.html', {'transaksi':transaksi})

def hapus_transaksi_view(request):
    if request.session.get('role') != "ADMIN":
        return redirect("/daftar-transaksi/")
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago;')
    if request.method == 'POST' and 'id_transaksi' in request.POST:
        cursor.execute(f"DELETE FROM Transaksi WHERE id_transaksi='{request.POST['id_transaksi']}';")
    return redirect("/daftar-transaksi")