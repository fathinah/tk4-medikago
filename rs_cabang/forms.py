from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute('set search_path to medikago')

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

class BuatRSCabang(forms.Form):
	kode_rs = forms.CharField(label = 'Kode RS', max_length = 50, required = True)
	nama = forms.CharField(label = 'Nama', max_length = 50, required = True)
	tanggal_pendirian = forms.DateField(label = 'Tanggal Pendirian', required = True, widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))
	jalan = forms.CharField(label = 'Jalan', required = False)
	kota = forms.CharField(label = 'Kota', required = True)
	nomor = forms.IntegerField(label = 'Nomor', required = False, min_value = 0)

class UpdateRSCabang(forms.Form):
	kode_rs = forms.CharField(label = 'Kode RS', widget=forms.TextInput(attrs={
        'readonly' : True
    }))
	nama = forms.CharField(label = 'Nama', max_length = 50, required = True)
	tanggal_pendirian = forms.DateField(label = 'Tanggal Pendirian', required = True, widget=forms.DateInput(attrs={
        'placeholder' : 'Day/Month/Year',
        'type' : 'date',
        'required': True,
    }))
	jalan = forms.CharField(label = 'Jalan', required = False)
	kota = forms.CharField(label = 'Kota', required = True)
	nomor = forms.IntegerField(label = 'Nomor', required = False, min_value = 0)

class DaftarkanDokter(forms.Form):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago')
	cursor.execute('select kode_rs from rs_cabang;')
	dict_kode = dictfetchall(cursor)
	KODE_RS = [tuple([row["kode_rs"], row["kode_rs"]]) for row in dict_kode]
	cursor.execute('select id_dokter from dokter;')
	dict_id = dictfetchall(cursor)
	ID_DOKTER = [tuple([row["id_dokter"], row["id_dokter"]]) for row in dict_id]
	id_dokter = forms.CharField(label = 'ID Dokter', widget = forms.Select(choices = ID_DOKTER), required = True)
	kode_rs = forms.CharField(label = 'Kode RS', widget = forms.Select(choices = KODE_RS), required = True)

class UpdateDokter(forms.Form):
	cursor = connection.cursor()
	cursor.execute('set search_path to medikago')
	cursor.execute('select kode_rs from rs_cabang;')
	dict_kode = dictfetchall(cursor)
	KODE_RS = [tuple([row["kode_rs"], row["kode_rs"]]) for row in dict_kode]
	id_dokter = forms.CharField(label = 'ID Dokter', disabled = True, required = False)
	kode_rs = forms.CharField(label = 'Kode RS', widget = forms.Select(choices = KODE_RS), required = True)
