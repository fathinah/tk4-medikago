from django import forms

class ObatForm(forms.Form):
    kode_obat = forms.CharField(label = 'Kode obat', max_length = 50, required = True)
    stok = forms.IntegerField(label = 'Stok', required = True, min_value = 1)
    harga = forms.IntegerField(label = 'Harga', required = True, min_value = 1)
    komposisi = forms.CharField(label = 'Komposisi', required = False)
    bentuk_sediaan = forms.CharField(label = 'Bentuk sediaan', max_length = 50, required = True)
    merk_dagang = forms.CharField(label = 'Merk dagang', max_length = 50, required = True)

class UpdateObatForm(forms.Form):
    kode_obat = forms.CharField(label = 'Kode obat', widget=forms.TextInput(attrs={
        'readonly' : True
    }))
    stok = forms.IntegerField(label = 'Stok', required = True, min_value = 1)
    harga = forms.IntegerField(label = 'Harga', required = True, min_value = 1)
    komposisi = forms.CharField(label = 'Komposisi', required = False)
    bentuk_sediaan = forms.CharField(label = 'Bentuk sediaan', max_length = 50, required = True)
    merk_dagang = forms.CharField(label = 'Merk dagang', max_length = 50, required = True)