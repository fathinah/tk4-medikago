from django.shortcuts import render, redirect
from .forms import ObatForm, UpdateObatForm
from django.http import HttpResponse
from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def daftarobat_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        role = "admin"
    else:
        role = "others"

    cursor.execute("select * from obat;")
    rows = dictfetchall(cursor)
    return render(request, 'obat/daftarobat.html', {'rows':rows, 'role':role})

def addobat_view(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = ObatForm(request.POST)
            if form.is_valid():
                kode = form.cleaned_data.get('kode_obat')
                stok = form.cleaned_data.get('stok')
                harga = form.cleaned_data.get('harga')
                komposisi = form.cleaned_data.get('komposisi')
                bentuk_sediaan = form.cleaned_data.get('bentuk_sediaan')
                merk_dagang = form.cleaned_data.get('merk_dagang')
                try:
                    cursor.execute("insert into obat(kode, stok, harga, komposisi, bentuk_sediaan, merk_dagang) values('" + str(kode) + "'," + str(stok) + "," + str(harga) + ",'" + komposisi + "','" + bentuk_sediaan + "','" + merk_dagang + "');")
                    return redirect("/daftar-obat/")
                except:
                    error = "Obat dengan kode " + kode + " sudah ada"
                    context = {
                        'form' : form,
                        'error' : error
                    }
                    return render(request, 'obat/createobat.html', context)
            else:
                error = "Data yang anda masukkan tidak valid"
                context = {
                        'form' : form,
                        'error' : error
                    }
                return render(request, 'obat/createobat.html', context)
        else:
            form = ObatForm()
            context = {
                'form' : form
            }
            return render(request, 'obat/createobat.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def updateobat_view(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        if request.method == 'POST':
            form = UpdateObatForm(request.POST, initial = {
                'kode_obat' : slug
            })
            if form.is_valid():
                stok = form.cleaned_data.get('stok')
                harga = form.cleaned_data.get('harga')
                komposisi = form.cleaned_data.get('komposisi')
                bentuk_sediaan = form.cleaned_data.get('bentuk_sediaan')
                merk_dagang = form.cleaned_data.get('merk_dagang')
                cursor.execute("update obat set stok=" + str(stok) + ", harga=" + str(harga) + ", komposisi='" + komposisi + "', bentuk_sediaan='" + bentuk_sediaan + "', merk_dagang='" + merk_dagang + "' where kode='" + str(slug) + "';")
                return redirect("/daftar-obat/")
            else:
                context = {
                    'form' : form
                }
                return render(request, 'obat/updateobat.html', context)
        else:
            form = UpdateObatForm(initial = {
                'kode_obat' : slug
            })
            context = {
                'form' : form
            }
            return render(request, 'obat/updateobat.html', context)
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)

def delete_obat_view(request, slug):
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')

    if request.session.get('role') == "ADMIN":
        cursor.execute("delete from obat where kode='" + str(slug) + "';")
        return redirect("/daftar-obat/")
    else:
        html = "<html><body>You are not authorized to view this page.</body></html>"
        return HttpResponse(html)