from django.urls import path
from . import views
from .views import create_layanan_poliklinik, layanan_poliklinik, jadwal_layanan_poliklinik, update_layanan_poliklinik, create_jadwal_layanan_poliklinik, update_jadwal_layanan_poliklinik,delete_layanan_poliklinik, delete_jadwal_layanan_poliklinik

appname = 'layanan_poliklinik'

urlpatterns = [
    path('create-layanan-poliklinik/', create_layanan_poliklinik, name='create_layanan_poliklinik'),
    path('layanan-poliklinik/', layanan_poliklinik, name='layanan_poliklinik'),
    path('jadwal-layanan-poliklinik/<id>/', jadwal_layanan_poliklinik, name='jadwal_layanan_poliklinik'),
    path('update-layanan-poliklinik/<id>/', update_layanan_poliklinik, name='update_layanan_poliklinik'),
    path('delete-layanan-poliklinik/<id>/', delete_layanan_poliklinik, name='delete_layanan_poliklinik'),
    path('create-jadwal-layanan-poliklinik/<id>/', create_jadwal_layanan_poliklinik, name='create_jadwal_layanan_poliklinik'),
    path('update-jadwal-layanan-poliklinik/<id_poli>/<id_jadwal>/', update_jadwal_layanan_poliklinik, name='update_jadwal_layanan_poliklinik'),
    path('delete-jadwal-layanan-poliklinik/<id_poli>/<id_jadwal>/', delete_jadwal_layanan_poliklinik, name='delete_jadwal_layanan_poliklinik'),
]