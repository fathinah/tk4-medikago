from django.apps import AppConfig


class LayananPoliklinikConfig(AppConfig):
    name = 'layanan_poliklinik'
