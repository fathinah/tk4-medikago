from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

appname = 'profil'

urlpatterns = [
    url(r'^profil/(?P<slug>[\w-]+)/$', profil_view, name='profil')
]